var gulp = require('gulp');
var Sass = require('gulp-sass');
var Del = require('del');
var ImageMin = require('gulp-imagemin');
var Uglify = require('gulp-uglify');
var Usemin = require('gulp-usemin');
var Rev = require('gulp-rev');
var CleanCss = require('gulp-clean-css');
var Flatmap = require('gulp-flatmap');
var BrowserSync = require('browser-sync');
var Htmlmin = require('gulp-htmlmin');
var Concat = require('gulp-concat');
const Server = BrowserSync.create();

var paths = {
    styles: {
        src: './css/*.scss',
        dest: 'dist/css/'
    },
    scripts: {
        src: './js/*.js',
        dest: 'dist/js/'
    }
};

function clean(){
    return Del(['dist']);
}

function styles() {
    return gulp.src(paths.styles.src)
    .pipe(Sass())
    .pipe(CleanCss())
}

function scripts() {
    return gulp.src(paths.scripts.src, { sourcemaps: true })
      .pipe(Uglify())
      .pipe(Concat('main.min.js'))
      .pipe(gulp.dest(paths.scripts.dest));
  }

function reload(done) {
    Server.reload();
    done();
}
  
function serve(done) {
    var files = ['./*.html', './css/*.css', './js/*.js', './images/*.{png, jpg, jpeg, gif}']
    Server.init(files,{
        server: {
        baseDir: './'
        }
    });
    done();
}
  
function imagemin(){
    return gulp.src('./images/*')
    .pipe(ImageMin({
        optimizationLevel: 3, 
		progressive: true, 
		interlaced: true    
    }))
    .pipe(gulp.dest('dist/images'));
}  

function usemin(){
    return gulp.src('./*.html')
    .pipe(Flatmap(function(stream, file){
        return stream
            .pipe(Usemin({
                css: [Rev()],
                html: [Htmlmin({ collapseWhitespace: true })],
                js: [Uglify(), Rev()],
                inlinejs: [Uglify()],
                inlinecss: [CleanCss(), 'concat']
            }))
    }))
    .pipe(gulp.dest('dist/'));
}

function copyfonts(){
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
    .pipe(gulp.dest('dist/fonts'));
}

function watch() {
    gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
    gulp.watch(paths.styles.src, gulp.series(styles, reload));
}

var build = gulp.series(clean, copyfonts, imagemin, usemin, serve, watch);

exports.styles = styles;
exports.clean = clean;
exports.scripts = scripts;
exports.copyfonts = copyfonts;
exports.imagemin = imagemin;
exports.watch = watch;
exports.usemin = usemin;
exports.reload = reload;
exports.serve = serve;
exports.build = build;
exports.default = build;