$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.popover-dismiss').popover({
        trigger: 'focus'
        })
    $('.carousel').carousel({
    interval: 3000
    })
    $('#contacto').on('show.bs.modal', function(e){
        console.log('El modal se esta mostrando')
        $('#contactoBtn1').removeClass('btn-outline-info')
        $('#contactoBtn1').addClass('btn-success')
        $('#contactoBtn1').prop('disabled',true)
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('El modal se esta mostró')
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('El modal se esta oculta')
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('El modal se esta ocultó')
        $('#contactoBtn1').prop('disabled',false)
        $('#contactoBtn1').removeClass('btn-success')
        $('#contactoBtn1').addClass('btn-outline-info')
    }); 
})